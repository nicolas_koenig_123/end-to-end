describe('Adapt general test suite', function() {
    const waiting = 3000;
    const timeout = 10000;

    beforeEach(function() {
        cy.visit('/index.html');    
    })

    it('Go to page General Testing Instructions', function() {
        cy.get('.menu-item__title-inner', { timeout: timeout })
        .click()
        .then(() => {
            cy.wait(waiting);
        })

    })

    it('Open Accordian', () => {
        cy.get('.menu-item__title-inner', { 
            multiple: true, 
            timeout: timeout,
        })
        .click()
        .then(() => {
            cy.get('.btn-next').click();
            cy.wait(waiting);
        })
        .then(() => {
            cy.get('.accordion__item-title-inner')
            .contains('Item with a graphic')
            .click()
            .then(() => {
                cy.wait(waiting);
            });
        })
        .then(() => {
            cy.get('.accordion__item-title-inner', { timeout: timeout })
            .contains('Item with a graphic - align-image-right')
            .click()
            .then(() => {
                cy.wait(waiting);
            });
        })
    })

    it('Pin drawer', () => {
        cy.get('.nav__menu-btn', { timeout: timeout })
        .click()
        .then(() => {
            cy.get('.pin__menu > .btn-icon').click();
            cy.wait(waiting);
        });
    })
})