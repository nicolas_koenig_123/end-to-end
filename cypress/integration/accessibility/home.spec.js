describe('A11y', () => {
    const waiting = 3000;
    const timeout = 10000;

    it('home page is accessible', () => {
      cy.checkPageA11y('/index.html');
    });

    it('General Testing Instructions page is accessible', () => {
      cy.get('.menu-item__inner', { timeout: timeout })
      .click()
      .then(() => {
          cy.wait(waiting);
          cy.checkCurrentPageA11y();
      });
    });
  });